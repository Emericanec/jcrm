package ru.roque.crm.service;


import ru.roque.crm.model.User;

public interface UserService {

    void save(User user);

    User findByLogin(String login);
}
