package ru.roque.crm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.roque.crm.dao.RoleDao;
import ru.roque.crm.dao.UserDao;
import ru.roque.crm.model.Role;
import ru.roque.crm.model.User;

import java.util.HashSet;
import java.util.Set;

public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<Role>();
        roles.add(roleDao.findOne(1));
        user.setRoles(roles);
        userDao.save(user);
    }

    public User findByLogin(String login) {
        return userDao.findByLogin(login);
    }

}
