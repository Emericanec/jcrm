package ru.roque.crm.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.roque.crm.model.Role;


public interface RoleDao extends JpaRepository<Role, Integer> {
}
