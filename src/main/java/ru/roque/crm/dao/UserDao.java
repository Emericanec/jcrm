package ru.roque.crm.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.roque.crm.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

    @Query("select u from User as u where u.login = :login")
    User findByLogin(@Param("login") String login);
}
